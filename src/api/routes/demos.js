const express = require('express');
const demos = require('../services/demos');

const router = new express.Router();

/**
 * Returns all demos from the system that the user has access 
 * to
 */
router.get('/', async (req, res, next) => {
  const options = {
  };

  try {
    const result = await demos.getDemos(options);
    res.status(result.status || 200).send(result.data);
  } catch (err) {
    return res.status(500).send({
      status: 500,
      error: 'Server Error'
    });
  }
});

/**
 * Creates a new demo in the store.  Duplicates are allowed
 */
router.post('/', async (req, res, next) => {
  const options = {
    demo: req.body.demo
  };

  try {
    const result = await demos.postDemos(options);
    res.status(result.status || 200).send(result.data);
  } catch (err) {
    return res.status(500).send({
      status: 500,
      error: 'Server Error'
    });
  }
});

/**
 * Returns a user based on a single ID, if the user does not 
 * have access to the demo
 */
router.get('/:id', async (req, res, next) => {
  const options = {
    id: req.params.id
  };

  try {
    const result = await demos.getDemosById(options);
    res.status(result.status || 200).send(result.data);
  } catch (err) {
    return res.status(500).send({
      status: 500,
      error: 'Server Error'
    });
  }
});

/**
 * deletes a single demo based on the ID supplied
 */
router.delete('/:id', async (req, res, next) => {
  const options = {
    id: req.params.id
  };

  try {
    const result = await demos.deleteDemo(options);
    res.status(result.status || 200).send(result.data);
  } catch (err) {
    return res.status(500).send({
      status: 500,
      error: 'Server Error'
    });
  }
});

module.exports = router;
